<?php

//запрет прямого обращения к файлу с адрессной строки
define('CATALOG', TRUE);

header("Content-Type:text/html;charset=utf-8");

//error_reporting(0);//незначительные ошибки не будут выводиться

session_start();

//подключение файла конфигурации (config.php) 
require_once 'config.php';

//настройки пути для подключаемых файлов
set_include_path(get_include_path().PATH_SEPARATOR.CONTROLLER.PATH_SEPARATOR.MODEL.PATH_SEPARATOR.LIB);

//ф-ция автоматической загрузки классов
function __autoload($class_name){
    if(!include_once($class_name.".php")){
        try {
            throw new ContrException('Неправильный файл для подключения!');
        } catch (ContrException $e) {
            echo $e->getMessage();
        }
    }
}

try {
    $obj = Route_Controller::get_instance();
    $obj->route();
} 
catch (ContrException $e){
    return;
}

//@header("Location:".SITE_URL);
?>