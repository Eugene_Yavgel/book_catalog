<?php
class Model_Driver{
    static $instance;  
    public $ins_db; // объект БД    

    static function get_instance(){
        if(self::$instance instanceof self) {
            return self::$instance;
        }
        return self::$instance = new self;
    }
    
    public function __construct() {
        $this->ins_db = new mysqli(HOST,USER,PASS,DB);
        if($this->ins_db->connect_error){
            throw new DbException("Ошибка соединения: ".$this->ins_db->connect_errno." | ".  iconv("CP1251","UTF-8",$this->ins_db->connect_error));
        }
        $this->ins_db->query("SET NAMES 'UTF8'");
    }
    
    //выборка данных из БД
    public function select(
                            $param,
                            $table,
                            $where=array(),
                            $order=false,
                            $napr="ASC",
                            $limit=false,
                            $operand=array("="),
                            $match=array()
                            ) {
        $sql = "SELECT";       
        
        foreach ($param as $item) {
            $sql .= ' '.$item.','; 
        }
        
        $sql = rtrim($sql, ','); // убираем запятую
        
        if(is_array($table)){
           $sql .= ' FROM '.  implode(',',$table); 
        }else{
            $sql .= ' FROM '.$table;
        }        
        
        if (count($where) > 0){
            $ii = 0;
            foreach ($where as $key => $value) {
                if($ii==0){
                    if($operand[$ii] == "IN"){
                        $sql .= ' WHERE '.strtolower($key).' '.$operand[$ii].'('.$value.')';
                    } else {
                        $sql .= ' WHERE '.strtolower($key).' '.$operand[$ii].' '.$this->ins_db->real_escape_string($value);
                    }                    
                }
                if($ii > 0){
                    if ($operand[$ii] == "IN"){
                       $sql .= ' AND '.strtolower($key).' '.$operand[$ii].'('.$value.')';
                    } else {
                       $sql .= ' AND '.strtolower($key).' '.$operand[$ii].' '.$this->ins_db->real_escape_string($value);  
                    }                   
                }
                $ii++;
                if((count($operand) - 1) < $ii){ // если следующие операнды одинаковые(чтобы не перечислять все операнды)
                    $operand[$ii] = $operand[$ii-1];
                }
            } 
        }
        
            if (count($match) > 0){
                foreach ($match as $key => $value) {
                    if(count($where) > 0){
                         $sql .= " AND MATCH (".$key.") AGAINST('".$this->ins_db->real_escape_string($value)."')";
                    }elseif (count($where) == 0) {
                         $sql .= " WHERE MATCH (".$key.") AGAINST('".$this->ins_db->real_escape_string($value)."')";  
                    }
                }
            } 
            
            if($order){
                $sql .= ' ORDER BY '.$order.' '.$napr.' ';
            }
            
            if($limit){
                $sql .= ' LIMIT '.$limit;
            }
            
            $result = $this->ins_db->query($sql);            
            
            //проверка на ошибку запроса
            if(!$result){
                throw new DbException("Ощибка запроса!".$this->ins_db->connect_errno." | ".$this->ins_db->connect_error. "|" . $sql);
            }
            
            //посчитаем кол-во полей, кот-е вернет нам БД как результат
            if($result->num_rows == 0){
                return false;
            }
            for($i=0; $i<$result->num_rows; $i++){
                $row[] = $result->fetch_assoc();
            } 
            return $row;    
    }
    
    //удаление данных из БД
    public function delete(
                            $table,
                            $where=array(),
                            $operand=array('=')                            
                            ) {
        $sql = "DELETE FROM ".$table;
        
        if(is_array($where)){
            $i = 0;
            foreach ($where as $key => $value) {
               $sql .= ' WHERE '.$key.$operand[$i]."'".$value."'";
               $i++;
               
               if((count($operand) - 1) < $i){
                    $operand[$i] = $operand[$i-1];
                }
            }            
        }
        
        $result = $this->ins_db->query($sql);
        
        if(!$result){
            throw new DbException("Ошибка базы данных: ".$this->ins_db->errno." | ".$this->ins_db->error);
            return FALSE;
        }
        return TRUE;
    }
    
    //добавление данных из БД
    public function insert(
                            $table,
                            $data=array(),
                            $values=array(),
                            $id=FALSE
                            ) {
        $sql = "INSERT INTO ".$table." (";        
        $sql .= implode(',', $data).") ";        
        $sql .= "VALUES (";        
        foreach ($values as $val) {
            $sql .= "'".$val."'".",";
        }
        $sql = rtrim($sql,',').")";
        
        $this->ins_db->query($sql);        
        
        $result = $this->ins_db->insert_id; //получаем id последней добавленной записи
        
        if(!$result){
            throw new DbException("Ошибка базы данных: ".$this->ins_db->errno." | ".$this->ins_db->error.$sql);
            return FALSE;
        }       
        return $result;  
    }
    
    //обновление данных из БД
    public function update(
                            $table,
                            $data=array(),
                            $values=array(),
                            $where=array()
                            ){
        $data_res = array_combine($data, $values);
        
        $sql = "UPDATE ".$table." SET ";
                      
        foreach ($data_res as $key => $value) {
           $sql .= $key."='".$value."',";
        }
        
        $sql = rtrim($sql,',');
        
        foreach ($where as $k => $v) {
           $sql .= " WHERE ".$k."='".$v."'";
        }
        
        $result = $this->ins_db->query($sql);
        
        if(!$result){
            throw new DbException("Ошибка базы данных: ".$this->ins_db->errno." | ".$this->ins_db->error);
            return FALSE;
        }         
        return TRUE;
    }
    
    ######################################################################################################################    
    //выборка данных из БД для админстраницы
    public function select_adm(
                            $param,
                            $table,
                            $where=array(),
                            $order=false,
                            $napr="ASC",
                            $limit=false,
                            $operand=array("="),
                            $match=array()
                            ) {
        $sql = "SELECT";
        
        foreach ($param as $item) {
            $sql .= ' '.$item.','; 
        }
        
        $sql = rtrim($sql, ','); // убираем запятую
        $sql .= ' FROM ';
        
        foreach ($table as $i) {
            $sql .= ' '.$i.','; 
        }
        
        $sql = rtrim($sql, ','); // убираем запятую
        
        if (count($where) > 0){
            $ii = 0;
            foreach ($where as $key => $value) {
                if($ii==0){
                    if($operand[$ii] == "IN"){
                        $sql .= ' WHERE '.strtolower($key).' '.$operand[$ii].'('.$value.')';
                    } else {
                        $sql .= ' WHERE '.strtolower($key).' '.$operand[$ii].' '.$this->ins_db->real_escape_string($value);
                    }                    
                }
                if($ii > 0){
                    if ($operand[$ii] == "IN"){
                       $sql .= ' AND '.strtolower($key).' '.$operand[$ii].'('.$value.')';
                    } else {
                       $sql .= ' AND '.strtolower($key).' '.$operand[$ii].' '.$this->ins_db->real_escape_string($value);  
                    }                   
                }
                $ii++;
                if((count($operand) - 1) < $ii){ // если следующие операнды одинаковые(чтобы не перечислять все операнды)
                    $operand[$ii] = $operand[$ii-1];
                }
            } 
        }
        
            if (count($match) > 0){
                foreach ($match as $key => $value) {
                    if(count($where) > 0){
                         $sql .= " AND MATCH (".$key.") AGAINST('".$this->ins_db->real_escape_string($value)."')";
                    }elseif (count($where) == 0) {
                         $sql .= " WHERE MATCH (".$key.") AGAINST('".$this->ins_db->real_escape_string($value)."')";  
                    }
                }
            } 
            
            if($order){
                $sql .= ' ORDER BY '.$order.' '.$napr.' ';
            }
            
            if($limit){
                $sql .= ' LIMIT '.$limit;
            }
                        
            $result = $this->ins_db->query($sql);
            
            //проверка на ошибку запроса
            if(!$result){
                throw new DbException("Ощибка запроса!".$this->ins_db->connect_errno." | ".$this->ins_db->connect_error);
            }
            
            //посчитаем кол-во полей, кот-е вернет нам БД как результат
            if($result->num_rows == 0){
                return false;
            }
            for($i=0; $i<$result->num_rows; $i++){
                $row[] = $result->fetch_assoc();
            } 
            return $row;  
    }    
    ##################################################################################################################        
}
?>