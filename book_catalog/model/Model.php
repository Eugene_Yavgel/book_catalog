<?php

class Model{
  static $instance;
  public $ins_driver; //объект класса Model_Driver
  
  static function get_instance(){
      if(self::$instance instanceof self) {
          return self::$instance;
      }
      return self::$instance = new self;
  }
  
  private function __construct() {
      try {
          $this->ins_driver = Model_Driver::get_instance();
      } catch (DbException $e) {
          exit();
      }      
  }
  
  //вывод всех книг
  public function get_books($all = FALSE){
      if($all){
        $result = $this->ins_driver->select(
                                            array('book_id','book_name','img', 'price', 'info'),
                                            'books',
                                            array(),
                                            'book_name',
                                            'ASC'                                            
                                            );  
      }  else {
        $result = $this->ins_driver->select(
                                            array('book_id','book_name','img'),
                                            'books',
                                            array(),
                                            'book_name',
                                            'ASC',
                                            3
                                            );
      }    
     return $result;
  }
  
  //вывод списка жанров книг
  public function get_genres(){
      $result = $this->ins_driver->select(
                                          array('genre_id','genre_name'),
                                          'genre'                                          
                                          );
     return $result;
  }
  
  //вывод списка авторов книг
  public function get_authors(){
      $result = $this->ins_driver->select(
                                          array('author_id','author_name'),
                                          'author'
                                          );
      return $result;
  }  
  
   //получение массива описания товаров
  public function get_tovar($id){
      $result = $this->ins_driver->select(
                                          array('book_id','book_name','info','price','img'),
                                          'books',
                                          array('book_id' => $id)                                         
                                          );
      return $result[0];
  }  
  
  ################################################# АДМИНЧАСТЬ #############################################################
  
  //запрос для построения общей таблицы(админстраница)
  public function get_table(){
      $result = $this->ins_driver->select_adm
            (
             array('books.book_id as id', 'books.book_name as name', 'books.price as price', 'books.info as info', 'genre.genre_name as genre', 'author.author_name as author'),
             array('books', 'genre', 'author', 'genre_book', 'author_book'),
             array('books.book_id' => 'genre_book.book_id', 'genre_book.genre_id' => 'genre.genre_id', 'author_book.book_id' => 'books.book_id', 'author_book.author_id' => 'author.author_id'),
             'author',
             'ASC'
            );          
      return $result;
  }
  
  //формирование выпадающего списка категорий(админстраница)
  public function select_key($ar, $name, $flag, $sel) {
      echo "<select id=$name name=$name>";        
      if ($flag == false) {
          echo "<option selected value='null'>$sel</option>";
      }
        foreach ($ar as $key => $value) {
            if ($sel == $value && $flag == true) {
                echo "<option selected value ='$key'>$value</option>";
            }
            else {
                echo "<option value ='$key'>$value</option>";
            }
        }
        echo "</select>";
    }
    
    ##################################### ДОБАВЛЕНИЕ КНИГ #############################################################
    
    //добавление книги в БД
  public function add_book($title, $price, $info){
      $result = $this->ins_driver->insert(
                                          'books',
                                          array('book_name', 'price', 'info'),
                                          array($title, $price, $info)
                                          );
     return $result;
  }
  
  //добавление айди жанра и книги в БД
  public function add_book_genre($genre_id, $book_id){
      $result = $this->ins_driver->insert(
                                          'genre_book',
                                          array('genre_id', 'book_id'),
                                          array($genre_id, $book_id)
                                          );
      return $result;
  }
  
  //добавление айди автора и книги в БД
  public function add_book_author($author_id, $book_id){
      $result = $this->ins_driver->insert(
                                          'author_book',
                                          array('author_id', 'book_id'),
                                          array($author_id, $book_id)
                                          );
      return $result;
  }
  
  //добавление автора в БД
  public function add_author($author_name){
      $result = $this->ins_driver->insert(
                                          'author',
                                          array('author_name'),
                                          array($author_name)
                                          );
      return $result;
  }
  
  //добавление жанра в БД
  public function add_genre($genre_name){
      $result = $this->ins_driver->insert(
                                          'genre',
                                          array('genre_name'),
                                          array($genre_name)
                                          );
      return $result;
  }
  
  ##################################### ДОБАВЛЕНИЕ КНИГ ################################################################  
  
  
  ##################################### РЕДАКТИРОВАНИЕ КНИГ #############################################################
  
  //редактирование книг(админчасть)
  public function edit_book($id, $name, $price, $info){
      $result = $this->ins_driver->update(
                                          'books',
                                          array('book_id', 'book_name', 'price', 'info'),
                                          array($id, $name, $price, $info),
                                          array('book_id' => $id)
                                          );
      return $result;
  }
  
  //редактирование aйди книги(админчасть)
  public function edit_book_genre($id, $genre){
      $result = $this->ins_driver->update(
                                          'genre_book',
                                          array('genre_id'),
                                          array($genre),
                                          array('book_id' => $id)
                                          );
      return $result;
  }
  
  //редактирование aйди книги(админчасть)
  public function edit_book_author($id, $author){
      $result = $this->ins_driver->update(
                                          'author_book',
                                          array('author_id'),
                                          array($author),
                                          array('book_id' => $id)
                                          );
      return $result;
  } 
  
  ##################################### РЕДАКТИРОВАНИЕ КНИГ #############################################################  
  
  
  ##################################### УДАЛЕНИЕ КНИГ ###################################################################
  
  //удаление книги(админчасть)
  public function delete_book($id){
      $result = $this->ins_driver->delete(
                                          'books',
                                           array('book_id' => $id)
                                          );
      return $result;
  }
  
  //удаление aйди книги(админчасть)
  public function delete_book_genre($id){
      $result = $this->ins_driver->delete(
                                          'genre_book',
                                           array('book_id' => $id)
                                          );
      return $result;
  }
  
  //удаление aйди книги(админчасть)
  public function delete_book_author($id){
      $result = $this->ins_driver->delete(
                                          'author_book',
                                           array('book_id' => $id)
                                          );
      return $result;
  }
  
  ##################################### УДАЛЕНИЕ КНИГ ###################################################################  
}
?>