<td class="content">				
    <h2><b>Результаты поиска:</b></h2>
    <div class="kat_map">
        <a href="<?=SITE_URL;?>">Главная</a> / <span>Результаты поиска</span>
    </div>
    
    <?php if ($search): ?>       
        <?php foreach ($search as $item): ?>
            <div id="all_books">                
                <div class="books">                       
                    <a href="<?=SITE_URL;?>tovar/id/<?=$item['book_id'];?>"><img src="<?=SITE_URL.UPLOAD_DIR.$item['img'];?>" alt="<?=$item['book_name'];?>" /></a>
                    <a href="<?=SITE_URL;?>tovar/id/<?=$item['book_id'];?>"><?=$item['book_name'];?></a>
                </div>                
            </div>              
        <?php endforeach; ?>
    <div class="clr"></div>
                
    <!-- *************************** Постраничная навигация ************************* -->
    <?php if($navigation): ?>
        <ul class="pager">
            <?php if(@$navigation['first']): ?>
            <li class="first"><a href="<?=SITE_URL;?>search/page/1/str/<?=$str;?>">Первая</a></li>
            <?php endif; ?>

            <?php if(@$navigation['last_page']): ?>
            <li><a href="<?=SITE_URL;?>search/page/<?=$navigation['last_page'];?>/str/<?=$str;?>">&lt;</a></li>
            <?php endif; ?>

            <?php if(@$navigation['previous']): ?>
                <?php foreach ($navigation['previous'] as $value): ?>
                    <li><a href="<?=SITE_URL;?>search/page/<?=$value;?>/str/<?=$str;?>"><?=$value;?></a></li>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if(@$navigation['current']): ?>
                <li><span><?=$navigation['current'];?></span></li>
            <?php endif; ?>

            <?php if(@$navigation['next']): ?>
                <?php foreach ($navigation['next'] as $val): ?>
                    <li><a href="<?=SITE_URL;?>search/page/<?=$val;?>/str/<?=$str;?>"><?=$val;?></a></li>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php if(@$navigation['next_pages']): ?>
            <li><a href="<?=SITE_URL;?>search/page/<?=$navigation['next_pages'];?>/str/<?=$str;?>">&gt;</a></li>
            <?php endif; ?>

            <?php if(@$navigation['end']): ?>
            <li class="last"><a href="<?=SITE_URL;?>search/page/<?=$navigation['end'];?>/str/<?=$str;?>">Последняя</a></li>
            <?php endif; ?>            
        </ul>
    <?php endif; ?>
    <!-- *************************** Постраничная навигация ************************* -->
                
    <?php else: ?>  
        <p>Данных для вывода нет!</p>
    <?php endif; ?>
</td>
</body>
</html>