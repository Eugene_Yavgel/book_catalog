<td class="content">
    <h2><b>Название книг:</b></h2>
    <?php if($books): ?>
        <?php foreach($books as $item): ?>
             <!-- Вывод всех книг -->
            <div id="all_books">
                <div class="books">                                                               
                    <a href="<?=SITE_URL;?>tovar/id/<?=$item['book_id'];?>"><img src="<?=SITE_URL.UPLOAD_DIR.$item['img'];?>" alt="<?=$item['book_name'];?>" /></a>
                    <a href="<?=SITE_URL;?>tovar/id/<?=$item['book_id'];?>"><?=$item['book_name'];?></a>                            
                </div>
            </div>
        <?php endforeach; ?>
        <div class="clr"></div>
            
         <!-- *************************** Постраничная навигация ************************* -->
        <?php if($navigation): ?>
            <ul class="pager">
                <?php if(@$navigation['first']): ?>
                <li class="first"><a href="<?=SITE_URL;?>index/page/1">Первая</a></li>
                <?php endif; ?>
                
                <?php if(@$navigation['last_page']): ?>
                <li><a href="<?=SITE_URL;?>index/page/<?=$navigation['last_page'];?>">&lt;</a></li>
                <?php endif; ?>
                
                <?php if(@$navigation['previous']): ?>
                    <?php foreach ($navigation['previous'] as $value): ?>
                        <li><a href="<?=SITE_URL;?>index/page/<?=$value;?>"><?=$value;?></a></li>
                    <?php endforeach; ?>
                <?php endif; ?>
                
                <?php if(@$navigation['current']): ?>
                    <li><span><?=$navigation['current'];?></span></li>
                <?php endif; ?>
                    
                <?php if(@$navigation['next']): ?>
                    <?php foreach ($navigation['next'] as $val): ?>
                        <li><a href="<?=SITE_URL;?>index/page/<?=$val;?>"><?=$val;?></a></li>
                    <?php endforeach; ?>
                <?php endif; ?>
                
                <?php if(@$navigation['next_pages']): ?>
                <li><a href="<?=SITE_URL;?>index/page/<?=$navigation['next_pages'];?>">&gt;</a></li>
                <?php endif; ?>
                
                <?php if(@$navigation['end']): ?>
                <li class="last"><a href="<?=SITE_URL;?>index/page/<?=$navigation['end'];?>">Последняя</a></li>
                <?php endif; ?>            
            </ul>
        <?php endif; ?>
        <!-- *************************** Постраничная навигация ************************* -->

    <?php else: ?>
        <p>Запрашиваемой книги нет в наличии.</p>
    <?php endif; ?>     
</td>
</body>
</html>