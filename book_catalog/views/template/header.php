<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <!-- автоматически подключаем файлы стилей -->
  <?php if ($styles): ?>
      <?php foreach ($styles as $style): ?>
          <link rel="stylesheet" type="text/css" href="<?=$style;?>" />
      <?php endforeach; ?>
  <?php endif; ?>
  <title><?=$title;?></title>
</head>
<body>
<div class="header"><b>КНИЖНЫЙ КАТАЛОГ</b>
    
    <!-- поиск по названию книги -->
    <div id="search">
        <form method="POST" action="<?=SITE_URL;?>search">
            <p><a href="<?=SITE_URL;?>index">Показать все книги</a></p>
            <b>Поиск:</b>
            <input name="search_value" type="text" placeholder="не менее 3-х символов">                         
        </form>         
    </div>
</div><hr>