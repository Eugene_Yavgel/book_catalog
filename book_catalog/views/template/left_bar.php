<td class="nav">
    <h2>Список книг по жанру:</h2>
    <?php if($genres): ?>
        <div id="list_genres">
            <?php foreach ($genres as $item): ?>
                 <a href="<?=SITE_URL;?>catalog/genre/<?=$item['genre_id'];?>"><?=$item['genre_name'];?></a><br />
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    
    <h2>Список книг по автору:</h2>
    <?php if($authors): ?>
        <div id="list_authors">
            <?php foreach ($authors as $item): ?>
                 <a href="<?=SITE_URL;?>catalog/author/<?=$item['author_id'];?>"><?=$item['author_name'];?></a><br />
            <?php endforeach; ?>
        </div>
    <?php endif; ?>    
</td>