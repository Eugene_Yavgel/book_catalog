<td class="content">
    <h2>Подробнее о выбранной книге:</h2>
    <?php if($tovar): ?>
        <p><b><?=$tovar['book_name'];?></b></p>
       
       <div class="kat_map">
            <?php if(count($krohi) == 1): ?>
                <a href="<?=SITE_URL;?>">Главная</a> /
                <span><?=$krohi[0]['tovar_name'];?></span>            
            <?php endif; ?>            
        </div>
       
       <p><img src="<?=SITE_URL.UPLOAD_DIR.$tovar['img'];?>" class="img_book"></p>
       <p><b>Цена:</b> <span><?=$tovar['price'];?></span> грн.</p>
       <p><b>Аннотация:</b> <?=$tovar['info'];?></p>    
    <?php else: ?>
        <p>Данных с такими параметрами не существует!</p>
    <?php endif; ?> 
        
    <div class="order_book">
        <!-- Форма заказа книги клиентом -->
        <form class="order" method="POST" action="">
            <h3 id="h">Форма заказа:</h3><hr>
            <p><label>ФИО:</label><input type="text" name="order_book[fio]" /></p>
            <p><label>Адрес:</label><input type="text" name="order_book[address]" /></p>
            <p><label>Электронная почта:</label><input type="email" name="order_book[email]" /></p>
            <p><label>Количество экземпляров:</label><input type="text" name="order_book[quantity]" /></p>
            <p id="subres"><input type="submit" value="Заказать" name="submit" />
            <input type="reset" value="Сбросить" name="reset" /></p>      
        </form>
    </div>
    
    <?php 
    //Сообщение о заказе
    if(isset($_POST['order_book'])){
    $fio=$_POST['order_book']['fio'];
    $address=$_POST['order_book']['address'];
    $email=$_POST['order_book']['email'];
    $quantity=$_POST['order_book']['quantity'];
    $book_name=$tovar['book_name'];
    $id=$tovar['book_id'];

      // отправка письма администратору
      $to = 'admin@yandex.ua';
      $subject = 'Сообщение о заказе';
      $msg = "Даные про заказ книги:\n" .
      "ФИО клиента: $fio\n" .
      "Адрес клиента: $address\n" .
      "ID книги: $id\n" .
      "Название книги: $book_name\n" .
      "Количество книг: $quantity";
      mail($to, $subject, $msg, 'From:' . $email);

      // уведомление клиента об успешном заполнении формы
      echo "<p id='message'>Спасибо за заполнение формы, " . $fio . "!<br />" . 
                           " Ваша форма зарегистрирована.<br /> 
                           Стоимость заказа - " . $quantity*$tovar['price'] . " грн.</p>";  
    }
    ?>
    
</td>
</body>
</html>