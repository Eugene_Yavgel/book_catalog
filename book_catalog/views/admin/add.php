<td class="content"> 
  <p><a href="<?=SITE_URL;?>admin">На админстраницу</a></p>
  
  <h2>Добавление новой книги в каталог</h2>
    <?php if($mes): ?>
        <p id="mes"><?=$mes;?></p>
    <?php endif; ?>
  <!-- форма для добавления нов книги -->
  <?php if($authors && $genres): ?>
    <form method="POST" action="<?=SITE_URL;?>add" class="add_new_book">
        <h3 id="h">Добавить книгу:</h3><hr>
        
        <p><label>Автор:</label>
            <select name="author">
                <option selected="selected" value=""></option>
              <?php foreach ($authors as $value): ?>
                <option value="<?=$value['author_id'];?>"><?=$value['author_name'];?></option>                   
              <?php endforeach; ?>
            </select>
        </p>
        
        <p><label>Добавить автора:</label><input type="text" name="add_new_author"></p>
        <p><label>Название:</label><input type="text" name="title"></p>
        <p><label>Цена:</label><input type="text" name="price"></p>
        
        <p><label>Жанр:</label>
            <select id="s" name="genre">            
                <option selected="selected" value=""></option>
              <?php foreach ($genres as $value): ?>
                <option value="<?=$value['genre_id'];?>"><?=$value['genre_name'];?></option>                   
              <?php endforeach; ?>
            </select>
        </p>
        
        <p><label>Добавить жанр:</label><input type="text" name="add_new_genre"></p>
        <p><label>О книге:</label><textarea rows="10" cols="40" name="info" placeholder="Аннотация книги: не более 500 символов"></textarea></p>
        <p id="subres">
            <input type="submit" value="Добавить" name="submit" />
            <input type="reset" value="Сбросить" name="reset" />
        <p>
    </form>
  <?php endif; ?>
</td>
</body>
</html>