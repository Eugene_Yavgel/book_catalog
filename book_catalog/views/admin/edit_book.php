<td class="content">
    
    <p><a href="<?=SITE_URL;?>">На главную</a></p>
    <p><a href="<?=SITE_URL;?>add">Добавить новую книгу</a></p><br>
    
    <h2><b>Редактирование книжного каталога</b></h2> 
    
    <?php if($mes): ?>
        <p id="mes"><?=$mes;?></p>
    <?php endif; ?>
      
  <?php if($table && $authors && $genres):             
    // выпадающий список жанров         
    foreach ($genres as $value){ $genres_select[$value['genre_id']] = $value['genre_name']; }
    // выпадающий список авторов    
    foreach ($authors as $value){ $authors_select[$value['author_id']] = $value['author_name']; }   
  ?>
    
    <form method="POST" action="<?=SITE_URL;?>admin" class="edit_book">
        <table>
            <tr><th>Автор</th><th>Название книги</th><th>Цена</th><th>Жанр</th><th>Информация о книге</th><th>Действия</th></tr>
      
            <?php foreach($table as $t): ?>
                    <tr>
                <?php if($flag==$t['id']): ?>           
                    <td>
                        <?php $this->ob_m->select_key($authors_select, 'edit[author]', true, $t['author']); ?>
                    </td>
                    <span><input type="hidden" name=edit[id] value="<?=$t['id'];?>"></span>
                    <td><input type='text' name=edit[name] value="<?=$t['name'];?>"></td> 
                    <td><input type='text' name=edit[price] value="<?=$t['price'];?>"></td>
                    <td>
                        <?php $this->ob_m->select_key($genres_select, 'edit[genre]', true, $t['genre']); ?>
                    </td>
                    <td><textarea rows='3' cols='60' name=edit[info]><?=$t['info'];?></textarea></td>
                    <td><input type='submit' value='Редактировать' name=edit[submit]/></td>
                <?php else: ?>      
                    <td><?=$t['author'];?></td>
                    <td><?=$t['name'];?></td>
                    <td><?=$t['price'];?></td>
                    <td><?=$t['genre'];?></td>
                    <td><?= iconv('windows-1251', 'UTF-8',(substr(iconv('UTF-8', 'windows-1251', $t['info']), 0, 100)));?>...</td>
                    <td><a href="<?=SITE_URL;?>admin/id/<?=$t['id'];?>">Редактировать</a>
                        / <a href="<?=SITE_URL;?>admin/id/<?=$t['id'];?>/option/delete">Удалить</a>                        
                <?php endif; ?>               
                    </td></tr>              
            <?php endforeach; ?>
        </table><p></p>
    </form>    
  <?php endif; ?>    
</td>
</body>
</html>