<?php

// проверка на запрет прямого обращения к файлу с адрессной строки
defined('CATALOG') or die('Access denied! / Доступ запрещен!');

//путь к сайту
define('SITE_URL', '/book_catalog/');

//модель
define('MODEL', 'model');

//контроллер
define('CONTROLLER', 'controller');

//виды
define('VIEW', 'views/template/');

//виды(админка)
define('VIEW_ADMIN', 'views/admin/');

//библиотека
define('LIB', 'lib');

//путь к изображениям
define('UPLOAD_DIR', 'img/');

//кол-во выводимых товаров(для постраничной навигации)
define('QUANTITY', 10);

//кол-во выводимых ссылок на переход по страницам(для постраничной навигации)
define('QUANTITY_LINKS', 3);

//сервер БД
define('HOST', 'localhost');
//пользователь БД
define('USER', 'root');
//пароль БД
define('PASS', '');
//имя БД
define('DB', 'book_catalog');

//размер картинок
define('IMG_WIDTH', 116);

//доп настройки(авто подключение таблиц стилей-.css)
$conf = array(
              'styles' => array(
                                'style.css'                   
                               ),
              'styles_admin' => array(
                                      'style.css'                   
                                     )              
             );
?>