<?php

class Route_Controller extends Base_Controller{
//класс построен по шаблону Синглтон - позволяет создавать лишь один объект данного класса
    static $_instance;
    static function get_instance(){
        if(self::$_instance instanceof self){ //проверяем хранится ли в свойстве(переменной) $_instance  обеект класса self (Route_Controller)
            return self::$_instance; //если храниться - возвращаем объект данного класса, т.е. значение, хранящееся в $_instance
        }
        return self::$_instance = new self; //если нет - создаем и тут же возвращаем объект данного класса 
    }
    
    private function __construct() {
        $zapros = $_SERVER['REQUEST_URI'];
        $path = substr($_SERVER['PHP_SELF'], 0, strpos($_SERVER['PHP_SELF'], 'index.php')); //получаем корневую папку /book_catalog/
        if($path === SITE_URL){
            $this->request_url = substr($zapros, strlen(SITE_URL)); //откидуем из адреса часть с названием корневой папки /book_catalog/
            $url = explode('/',rtrim($this->request_url,'/'));
            if (!empty($url[0])){
                $this->controller = ucfirst($url[0]).'_Controller';//делаем первую букву названий заглавной ($url[0] - контроллер)
            }else {
                $this->controller = "Index_Controller"; //если нет $url[0] - присваиваем стартовый контроллер 
            }
            $count = count($url);
            if(!empty($url[1])){
                $key=array();//нечетные - имена параметров адресной строки
                $value=array();//четные - значения параметров адресной строки
                for ($i=1; $i < $count; $i++){
                    if($i%2 !=0){
                        $key[]=$url[$i];
                    }else{
                        $value[]=$url[$i];
                    }
                }
                if(!$this->params = array_combine($key, $value)){
                   throw new ContrException("Неправильный адресс!",$zapros);
                }
            }
        }  else {
            try {
                throw new Exception('<p style="color:red">Неправильный адрес сайта!</p>');
            } catch (Exception $ex) {
                echo $ex->getMessage();
                exit();
            }
        }        
    }    
}

?>