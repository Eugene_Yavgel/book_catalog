<?php

class Index_Controller extends Base {    
    protected $books;
    protected $navigation;
    
    protected function input($param=array()){
        parent::input();
        
        if(isset($param['page'])){
            $page = $this->clear_int($param['page']);
            if($page == 0){
                $page = 1;
            }
        } else { $page = 1; }
        
        $this->title .= "Главная";
        $this->books = $this->ob_m->get_books();
        
        $pager = new Pagination(
                                $page,
                                'books',
                                array(),
                                'book_id',
                                'ASC',
                                QUANTITY,
                                QUANTITY_LINKS                                
                                );
        if (is_object($pager)) {
            $this->navigation = $pager->get_navigation();
            $this->books = $pager->get_posts();            
        }
    }    
    
    protected function output(){                 
        $this->content = $this->render(VIEW.'content', array(
                                                             'navigation' => $this->navigation,
                                                             'books' => $this->books
                                                             ));         
        
        $this->page = parent::output();
        return $this->page;         
    }     
}
?>