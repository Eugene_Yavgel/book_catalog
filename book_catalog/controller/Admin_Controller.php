<?php
class Admin_Controller extends Base_Admin{
    protected $table; //массив данных о книгах
    protected $authors; //массив авторов
    protected $genres; //массив жанров
    protected $message;
    protected $option;
    protected $flag;


    protected function input($param=array()) {
        parent::input();
        
        /*echo $this->ob_m->test();
        exit();*/
                
        $this->title .= 'Редактирование';        
        $this->table = $this->ob_m->get_table();
        $this->authors = $this->ob_m->get_authors();
        $this->genres = $this->ob_m->get_genres();        
        
        if(isset($param['id'])){
            $id = $this->clear_int($param['id']);
            $this->flag = $id; 
            $this->option = 'edit';
            
            if(@$param['option'] == 'delete'){
                $result = $this->ob_m->delete_book_genre($id);
                $result = $this->ob_m->delete_book_author($id);
                $result = $this->ob_m->delete_book($id);                
                
                if($result === TRUE){
                    $_SESSION['message'] = 'Запись успешно удалена!';
                }else{ $_SESSION['message'] = 'Ошибка при удалении данных!'; }                    
                    
                header("Location:".SITE_URL."admin");
                exit();
            }
        }                        
        
        if($this->is_post()){
            $id = $_POST['edit']['id'];
            $author = $_POST['edit']['author'];
            $name = $_POST['edit']['name'];
            $price = $_POST['edit']['price'];
            $genre = $_POST['edit']['genre'];
            $info = $_POST['edit']['info'];
            
            if($_POST['edit']['submit']){
                $result = $this->ob_m->edit_book(
                                                 $id, 
                                                 $name,
                                                 $price,
                                                 $info                                                                                         
                                                 );
                $result = $this->ob_m->edit_book_genre(
                                                       $id,                                                
                                                       $genre                                                                                                
                                                       );
                $result = $this->ob_m->edit_book_author(
                                                        $id,                                                
                                                        $author                                                                                                
                                                        );
                if($result === TRUE){
                    $_SESSION['message'] = 'Изменения сохранены!';
                }else{ $_SESSION['message'] = 'Ошибка изменения данных!'; }                    
                header("Location:".SITE_URL."admin");
                exit();
            }
        }        
        $this->message = @$_SESSION['message'];        
    }
    
    protected function output() {
        $this->content = $this->render(VIEW_ADMIN.'edit_book', array(
                                                                      'table' => $this->table,
                                                                      'authors' => $this->authors,
                                                                      'genres' => $this->genres,
                                                                      'option' => $this->option,
                                                                      'mes' => $this->message,
                                                                      'flag' => $this->flag                                                              
                                                                      ));                
        $this->page = parent::output();
        unset($_SESSION['message']);
        return $this->page;
    }
}
?>