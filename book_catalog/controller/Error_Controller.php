<?php
class Error_Controller extends Base_Error{
    
    protected function input($param=array()) {
        parent::input();
        
        $er = ''; //строка, кот-ю запишем в файл log.txt
        
        $arr = array();
        if(isset($param['mes'])){
            foreach ($param as $key => $value) {
                $value = $this->clear_str(rawurldecode($value));
                $arr[] = $value;
                
                $er .= $key.' - '.$value.'|';
            }
            
            if($_SESSION['error']){
                foreach ($_SESSION['error'] as $k => $v) {
                    $er .= $k.' - '.$v.'|';
                }
            }
            unset($_SESSION['error']);
            $this->error = $er;
            $this->message_err = $arr;
        }
    }
    
    protected function output() {
        $this->page = parent::output();
        return $this->page;
    }
}
?>