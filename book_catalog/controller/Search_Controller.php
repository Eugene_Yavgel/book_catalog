<?php
 class Search_Controller extends Base {
     protected $str; //строка поискового запроса
     protected $navigation;
     protected $search;

     protected function input($params = array()) {
         parent::input();
         
         //проверка для постраничной навигации
         if(isset($params['page'])){ $page = $this->clear_int($params['page']);
             if($page == 0){ $page = 1; }
         }else{ $page = 1; }
         
         //проверка для поисковой строки
         if (isset($params['str'])){ 
             $this->str = rawurldecode($this->clear_str($params['str'])); //очищаем и расшифровуем
         } elseif ($this->is_post()){
             $this->str = $this->clear_str($_POST['search_value']);
         }
         
         $this->title .= "Результаты поиска по запросу - ".$this->str;                  
         $pager = new Pagination(
                                $page,
                                'books',
                                array(),
                                'book_id',
                                'ASC',
                                QUANTITY,
                                QUANTITY_LINKS ,
                                array(),
                                array('book_name,info' => $this->str)
                                );
        if (is_object($pager)) {
            $this->navigation = $pager->get_navigation();
            $this->search = $pager->get_posts();
            $this->str = rawurlencode($this->str); //шифруем поисковый запрос
        }                
     }
     
     protected function output() {
         $this->content = $this->render(VIEW.'search_page', array(
                                                                  'search' => $this->search,
                                                                  'navigation' => $this->navigation,
                                                                  'str' => $this->str
                                                                  ));
         
         $this->page = parent::output();
         return $this->page;
     }
 }
?>