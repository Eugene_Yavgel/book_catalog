<?php

//основной контроллер
abstract class Base_Controller{
    protected $request_url; //имя кортроллера+имя параметра+значение параметра
    protected $controller; //имя контроллера
    protected $params; //массив значений параметров(имя параметра+значение параметра)
     protected $styles, $styles_admin; // таблицы стилей
    protected $error; //ощибки приложения/сайта
    protected $page; //полность готовая к выводу на экран страница

    //работает с данными, кот-е формирует Route_Controller
    public function route(){
        /* Используем спец расширение для РНР - Reflection. */
        
        //проверим существует ли класс, имя кот-го записано в свойстве $controller
        if(class_exists($this->controller)){ //class_exists - вернет true, если класс сущ-т
           $ref = new ReflectionClass($this->controller); //обращаемся к классу ReflectionClass и сохраним объект этого класса
           
           //проверим есть ли в классе с именем, кот-е хранится в $this->controller, метод request
           if($ref->hasMethod('request')){
               //проверим можно ли получить объект из класса, имя кот-го нах-ся в $this->controller
               if($ref->isInstantiable()){
                   $class = $ref->newInstance(); //получаем объект этого класса
                   $method = $ref->getMethod('request'); //получаем метод request
                   $method->invoke($class, $this->get_params()); //вызываем метод request на исполнение                   
               }
           }           
        } else { throw new ContrException('Такой страницы не существует!','Контроллер - '.$this->controller); }
    }    
    
    public function init(){
        global $conf;
        
        if(isset($conf['styles'])){
            foreach ($conf['styles'] as $style) {
                $this->styles[] = trim($style, '/');
            }
        }
        if(isset($conf['styles_admin'])){
            foreach ($conf['styles_admin'] as $style_admin) {
                $this->styles_admin[] = trim($style_admin, '/');
            }
        }        
    }    
    
    //возвращает данные по контроллеру
    protected function get_controller(){
        return $this->controller;
    }
    
    //возвращает массив параметров
    protected function get_params(){
        return $this->params;
    }
    
    // получение входных данных - будет переопределен в дочерних классах
    protected function input(){}
    
    // выводит на экран готовую страницу - будет переопределен в дочерних классах
    protected function output(){}
    
    // главный/финальный метод данного базового контроллера - генерирует остальные методы класса, запускае метод route()
    public function request($param=array()){
        $this->init();
        $this->input($param);
        $this->output();
        
        if(!empty($this->error)){
            $this->write_error($this->error);
        }
        
        $this->get_page();
    }
    
    //вывод страницы на экран
    public function get_page(){
        echo $this->page;        
    }
    
    //шаблонизатор сайта
    protected function render($path, $param=array()){
        extract($param);
        ob_start();
        
        if(!include($path.'.php')){
            throw new ContrException('Данного шаблона нет!');
        }
        return ob_get_clean();
    }
    
    //очистка данных: и строки и массивы строк
    public function clear_str($var){
        if(is_array($var)){ //если массив строк
            $row = array();
            foreach ($var as $key => $item){
                $row[$key] = trim(strip_tags($item));
            }
            return $row;
        }
        return trim(strip_tags($var)); //если просто строка
        
    }
    
    //очистка числовых данных
    public function clear_int($var){
        return (int)$var;
    }
    
    //проверка пришли ли данные POST-методом
    public function is_post(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            return true;
        }
        return false;      
    }
           
    //проверка наличия авторизации
    public function check_auth(){
        // Имя пользователя и его пароль для аутентификации
        $username = 'admin';
        $password = 'book';

        if(!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']) || 
          ($_SERVER['PHP_AUTH_USER'] != $username) || ($_SERVER['PHP_AUTH_PW'] != $password)) {
            // Имя пользователя/пароль не действительны для отправки НТТР-заголовков, подтверждающих аутентификацию
            header('HTTP/1.1 401 Unauthorized');
            header('WWW-Authenticate: Basic realm="Книжный каталог: админстраница"');
            exit('<h2>Book catalog</h2>Sorry, you must enter a valid user name and password to access this page.');
        }
    }
    
    //запись ошибок в файл log.txt
    public function write_error($err){
        $time = date("d-m-Y G:i:s");
        $str = "Fault: ".$time." - ".$err."\n\r";
        file_put_contents("log.txt", $str, FILE_APPEND); //запись данных в файл
    }
    
    //уменьшение картинок до необходимого размера
    public function img_resize($dest){}
    
}
?>