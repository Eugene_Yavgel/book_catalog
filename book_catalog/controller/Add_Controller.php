<?php
class Add_Controller extends Base_Admin{
    protected $authors; //массив авторов
    protected $genres; //массив жанров
    protected $message;
    
    
    protected function input() {
        parent::input(); 
        
        $this->title .= 'Добавление новой книги';
        
        $this->authors = $this->ob_m->get_authors();
        $this->genres = $this->ob_m->get_genres();
        
        if($this->is_post()){
            $author_id = $_POST['author'];
            $add_new_author = $_POST['add_new_author'];
            $title = $_POST['title'];
            $price = $_POST['price'];
            $genre_id = $_POST['genre'];
            $add_new_genre = $_POST['add_new_genre'];
            $info = $_POST['info'];
            
            if(!empty($add_new_author)){                    
                $author_name = $_POST['add_new_author'];                    
                $author_id = $this->ob_m->add_author(                                                                      
                                                  $author_name
                                                  );                    
            }
            
            if(!empty($add_new_genre)){               
               $genre_name = $_POST['add_new_genre'];                    
               $genre_id = $this->ob_m->add_genre(                                                                                                  
                                                  $genre_name
                                                  );                    
            }
            
            if(!empty($title) && !empty($price) && !empty($info) && !empty($author_id) && !empty($genre_id)){         
                $book_id = $this->ob_m->add_book(                                                    
                                                $title,
                                                $price,                                                    
                                                $info                                                
                                                );
                $result_genre = $this->ob_m->add_book_genre(                                                    
                                                $genre_id,
                                                $book_id                                                
                                                );
                $result_author = $this->ob_m->add_book_author(                                                    
                                                $author_id,
                                                $book_id                                                
                                                );
                if($result_author == TRUE && $result_genre == TRUE){
                    $_SESSION['message'] = 'Новая книга успешно добавлена!';
                }else{ $_SESSION['message'] = 'Ошибка добавления данных!'; }                    

                header("Location:".SITE_URL."add");
                exit();                                
            }
        }
        $this->message = @$_SESSION['message'];
    }
    
    protected function output() {
        $this->content = $this->render(VIEW_ADMIN.'add', array(
                                                                 'authors' => $this->authors,
                                                                 'genres' => $this->genres,
                                                                 'mes' => $this->message
                                                                 ));
        
        $this->page = parent::output();
        unset($_SESSION['message']);
        return $this->page;
    }
}
?>