<?php
abstract class Base_Admin extends Base_Controller{
    protected $ob_m; //объект модели
    protected $title;
    protected $style;   
    protected $content;
    protected $user = TRUE;   
    
    protected function input() {
        if($this->user == TRUE){ $this->check_auth(); }
        
        $this->title = "Книжный каталог: админстраница | ";
        
        foreach($this->styles_admin as $style) {
            $this->style[] = SITE_URL.VIEW_ADMIN.$style;           
        }
        		
        $this->ob_m = Model::get_instance();        
    }
    
    protected function output() {
        $header = $this->render(VIEW_ADMIN.'header', array(
                                                           'title' => $this->title,
                                                           'styles' => $this->style                                                           
                                                           ));               
        $page = $this->render(VIEW_ADMIN.'index', array(
                                                        'header' => $header,                                                           
                                                        'content' => $this->content                                                           
                                                        ));       
        return $page;
    }
}
?>