<?php

abstract class Base extends Base_Controller {
    protected $ob_m; //объект модели сайта
    protected $title; // заголовок каждой страницы сайта
    protected $style; // таблицы стилей
    protected $header; // шапка сайта
    protected $content; // вывод товара(средняя часть сайта)
    protected $left_bar; // левая часть сайта
    protected $pages, $genres, $authors;


    protected function input(){
        $this->title = "Книжный каталог | ";
        
        foreach ($this->styles as $style) {
            $this->style[] = SITE_URL.VIEW.$style;
        }
        
        $this->ob_m = Model::get_instance();
        
        $this->genres = $this->ob_m->get_genres(); //получаем массив жанров книг
        $this->authors = $this->ob_m->get_authors(); //получаем массив авторов книг       
    }    
    
    protected function output(){
        $this->left_bar = $this->render(VIEW.'left_bar', array(
                                                               'genres' => $this->genres,
                                                               'authors' => $this->authors
                                                               ));
        $this->header = $this->render(VIEW.'header', array(
                                                            'styles' => $this->style,                                                            
                                                            'title' => $this->title                                                            
                                                            ));        
        // собираем кусочки сайта в одно целое
        $page = $this->render(VIEW.'index', array(
                                                  'header' => $this->header,
                                                  'left_bar' => $this->left_bar,
                                                  'content' => $this->content                                                            
                                                  )); 
        return $page;
    }
}
?>