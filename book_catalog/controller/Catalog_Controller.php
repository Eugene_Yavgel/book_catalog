<?php
class Catalog_Controller extends Base {
    protected $type = FALSE; // книги по автору и по жанру
    protected $id = FALSE; //значения параметров(идентификаторы)
    protected $navigation; // массив ссылок для постраничной навигации
    protected $catalog; // массив книг для вывода на экран
    

    protected function input($param=array()) {
        parent::input();
        
        $this->title .= 'Каталог';        
        
        // принятие всевозможных параметров каталога
        if (isset($param['genre'])){
            $this->type = "genre";
            $this->id = $this->clear_int($param['genre']); //очищаем значение параметра
        }
        elseif (isset($param['author'])) {
            $this->type = "author";
            $this->id = $this->clear_int($param['author']); //очищаем значение параметра
        }
        
        // принятие номера страницы
        if (isset($param['page'])){
            $page = $this->clear_int($param['page']); //очищаем значение параметра
            if ($page == 0){
                $page = 1;
            }
        } else { $page = 1; }
 
        if ($this->type){
            if(!$this->id){ return; }
            if($this->type=='genre'){
                $pager = new Pagination(
                                        $page,
                                        array('books','genre','genre_book'),
                                        array('books.book_id'=>$this->type.'_book.book_id',$this->type.'_book.'.$this->type.'_id'=>$this->type.'.'.$this->type.'_id',$this->type.'.'.$this->type.'_id'=>$this->id),
                                        'book_name',
                                        'ASC',
                                        QUANTITY,
                                        QUANTITY_LINKS                                    
                                        );
            } else if($this->type=='author'){
                $pager = new Pagination(
                                        $page,
                                        array('books','author','author_book'),
                                        array('books.book_id'=>$this->type.'_book.book_id',$this->type.'_book.'.$this->type.'_id'=>$this->type.'.'.$this->type.'_id',$this->type.'.'.$this->type.'_id'=>$this->id),
                                        'book_name',
                                        'ASC',
                                        QUANTITY,
                                        QUANTITY_LINKS                                    
                                        );
            }
        }
        //вывод всех товаров
        else if (!$this->type) {
            $pager = new Pagination(
                                    $page,
                                    'books',
                                    array(), 
                                    'book_id',
                                    'ASC',
                                    QUANTITY,
                                    QUANTITY_LINKS                                    
                                    );            
        }
              
        if(is_object($pager)){
            $this->navigation = $pager->get_navigation();
            $this->catalog = $pager->get_posts();
        }        
    }
    
    protected function output() {        
        $previous = FALSE;
        if($this->type && $this->id){
            $previous = "/".$this->type."/".$this->id;
        }  
        
        $this->content = $this->render(VIEW.'catalog_page', array(
                                                                  'catalog' => $this->catalog,
                                                                  'navigation' => $this->navigation,
                                                                  'previous' => $previous                                                                  
                                                                  ));         
               
        $this->page = parent::output();
        return $this->page;
    }        
}
?>